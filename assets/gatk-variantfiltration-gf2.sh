#!/bin/bash

# GATK VariantFiltration wrapper script


###############################################################################
#### Helper Functions ####
###############################################################################

## ****************************************************************************
## Usage description should match command line arguments defined below
usage () {
    echo "Usage: $(basename "$0")"
    echo "  --input => VCF File"
    echo "  --reference_sequence => Reference Sequence Directory"
    echo "  --filter_expression => Filter Expression"
    echo "  --filter_name => Filter Name"
    echo "  --output => Output Directory"
    echo "  --exec_method => Execution method (cdc-shared-singularity, singularity, auto)"
    echo "  --exec_init => Execution initialization command(s)"
    echo "  --help => Display this help message"
}
## ****************************************************************************

# report error code for command
safeRunCommand() {
    cmd="$@"
    eval "$cmd; "'PIPESTAT=("${PIPESTATUS[@]}")'
    for i in ${!PIPESTAT[@]}; do
        if [ ${PIPESTAT[$i]} -ne 0 ]; then
            echo "Error when executing command #${i}: '${cmd}'"
            exit ${PIPESTAT[$i]}
        fi
    done
}

# print message and exit
fail() {
    msg="$@"
    echo "${msg}"
    usage
    exit 1
}

# always report exit code
reportExit() {
    rv=$?
    echo "Exit code: ${rv}"
    exit $rv
}

trap "reportExit" EXIT

# check if string contains another string
contains() {
    string="$1"
    substring="$2"

    if test "${string#*$substring}" != "$string"; then
        return 0    # $substring is not in $string
    else
        return 1    # $substring is in $string
    fi
}



###############################################################################
## SCRIPT_DIR: directory of current script, depends on execution
## environment, which may be detectable using environment variables
###############################################################################
if [ -z "${AGAVE_JOB_ID}" ]; then
    # not an agave job
    SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
else
    echo "Agave job detected"
    SCRIPT_DIR=$(pwd)
fi
## ****************************************************************************



###############################################################################
#### Parse Command-Line Arguments ####
###############################################################################

getopt --test > /dev/null
if [ $? -ne 4 ]; then
    echo "`getopt --test` failed in this environment."
    exit 1
fi

## ****************************************************************************
## Command line options should match usage description
OPTIONS=
LONGOPTIONS=help,exec_method:,exec_init:,input:,reference_sequence:,filter_expression:,filter_name:,output:,
## ****************************************************************************

# -temporarily store output to be able to check for errors
# -e.g. use "--options" parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(\
    getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@"\
)
if [ $? -ne 0 ]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    usage
    exit 2
fi

# read getopt's output this way to handle the quoting right:
eval set -- "$PARSED"

## ****************************************************************************
## Set any defaults for command line options
FILTER_NAME="filter"
EXEC_METHOD="auto"
EXEC_INIT=""
## ****************************************************************************

## ****************************************************************************
## Handle each command line option. Lower-case variables, e.g., ${file}, only
## exist if they are set as environment variables before script execution.
## Environment variables are used by Agave. If the environment variable is not
## set, the Upper-case variable, e.g., ${FILE}, is assigned from the command
## line parameter.
while true; do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --input)
            if [ -z "${input}" ]; then
                INPUT=$2
            else
                INPUT=${input}
            fi
            shift 2
            ;;
        --reference_sequence)
            if [ -z "${reference_sequence}" ]; then
                REFERENCE_SEQUENCE=$2
            else
                REFERENCE_SEQUENCE=${reference_sequence}
            fi
            shift 2
            ;;
        --filter_expression)
            if [ -z "${filter_expression}" ]; then
                FILTER_EXPRESSION=$2
            else
                FILTER_EXPRESSION=${filter_expression}
            fi
            shift 2
            ;;
        --filter_name)
            if [ -z "${filter_name}" ]; then
                FILTER_NAME=$2
            else
                FILTER_NAME=${filter_name}
            fi
            shift 2
            ;;
        --output)
            if [ -z "${output}" ]; then
                OUTPUT=$2
            else
                OUTPUT=${output}
            fi
            shift 2
            ;;
        --exec_method)
            if [ -z "${exec_method}" ]; then
                EXEC_METHOD=$2
            else
                EXEC_METHOD=${exec_method}
            fi
            shift 2
            ;;
        --exec_init)
            if [ -z "${exec_init}" ]; then
                EXEC_INIT=$2
            else
                EXEC_INIT=${exec_init}
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option"
            usage
            exit 3
            ;;
    esac
done
## ****************************************************************************

## ****************************************************************************
## Log any variables passed as inputs
echo "Input: ${INPUT}"
echo "Reference_sequence: ${REFERENCE_SEQUENCE}"
echo "Filter_expression: ${FILTER_EXPRESSION}"
echo "Filter_name: ${FILTER_NAME}"
echo "Output: ${OUTPUT}"
echo "Execution Method: ${EXEC_METHOD}"
echo "Execution Initialization: ${EXEC_INIT}"
## ****************************************************************************



###############################################################################
#### Validate and Set Variables ####
###############################################################################

## ****************************************************************************
## Add app-specific logic for handling and parsing inputs and parameters

# INPUT input

if [ -z "${INPUT}" ]; then
    echo "VCF File required"
    echo
    usage
    exit 1
fi
# make sure INPUT is staged
count=0
while [ ! -f "${INPUT}" ]
do
    echo "${INPUT} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -f "${INPUT}" ]; then
    echo "VCF File not found: ${INPUT}"
    exit 1
fi
INPUT_FULL=$(readlink -f "${INPUT}")
INPUT_DIR=$(dirname "${INPUT_FULL}")
INPUT_BASE=$(basename "${INPUT_FULL}")


# REFERENCE_SEQUENCE input

if [ -z "${REFERENCE_SEQUENCE}" ]; then
    echo "Reference Sequence Directory required"
    echo
    usage
    exit 1
fi
# make sure REFERENCE_SEQUENCE is staged
count=0
while [ ! -d "${REFERENCE_SEQUENCE}" ]
do
    echo "${REFERENCE_SEQUENCE} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -d "${REFERENCE_SEQUENCE}" ]; then
    echo "Reference Sequence Directory not found: ${REFERENCE_SEQUENCE}"
    exit 1
fi
REFERENCE_SEQUENCE_FULL=$(readlink -f "${REFERENCE_SEQUENCE}")
REFERENCE_SEQUENCE_DIR=$(dirname "${REFERENCE_SEQUENCE_FULL}")
REFERENCE_SEQUENCE_BASE=$(basename "${REFERENCE_SEQUENCE_FULL}")



# FILTER_EXPRESSION parameter
if [ -n "${FILTER_EXPRESSION}" ]; then
    :
else
    :
    echo "Filter Expression required"
    echo
    usage
    exit 1
fi


# FILTER_NAME parameter
if [ -n "${FILTER_NAME}" ]; then
    :
else
    :
fi


# OUTPUT parameter
if [ -n "${OUTPUT}" ]; then
    :
    OUTPUT_FULL=$(readlink -f "${OUTPUT}")
    OUTPUT_DIR=$(dirname "${OUTPUT_FULL}")
    OUTPUT_BASE=$(basename "${OUTPUT_FULL}")
    LOG_FULL="${OUTPUT_DIR}/_log"
    TMP_FULL="${OUTPUT_DIR}/_tmp"
else
    :
    echo "Output Directory required"
    echo
    usage
    exit 1
fi

## ****************************************************************************

## EXEC_METHOD: execution method
## Suggested possible options:
##   auto: automatically determine execution method
##   singularity: singularity image packaged with the app
##   docker: docker containers from docker-hub
##   environment: binaries available in environment path

## ****************************************************************************
## List supported execution methods for this app (space delimited)
exec_methods="cdc-shared-singularity singularity auto"
## ****************************************************************************

## ****************************************************************************
# make sure the specified execution method is included in list
if ! contains " ${exec_methods} " " ${EXEC_METHOD} "; then
    echo "Invalid execution method: ${EXEC_METHOD}"
    echo
    usage
    exit 1
fi
## ****************************************************************************



###############################################################################
#### App Execution Initialization ####
###############################################################################

## ****************************************************************************
## Execute any "init" commands passed to the GeneFlow CLI
CMD="${EXEC_INIT}"
echo "CMD=${CMD}"
safeRunCommand "${CMD}"
## ****************************************************************************



###############################################################################
#### Auto-Detect Execution Method ####
###############################################################################

# assign to new variable in order to auto-detect after Agave
# substitution of EXEC_METHOD
AUTO_EXEC=${EXEC_METHOD}
## ****************************************************************************
## Add app-specific paths to detect the execution method.
if [ "${EXEC_METHOD}" = "auto" ]; then
    # detect execution method
    if command -v singularity >/dev/null 2>&1 && [ -f "/apps/standalone/singularity/gatk/gatk-4.1.4.1-biocontainers.simg" ]; then
        AUTO_EXEC=cdc-shared-singularity
    elif command -v singularity >/dev/null 2>&1; then
        AUTO_EXEC=singularity
    else
        echo "Valid execution method not detected"
        echo
        usage
        exit 1
    fi
    echo "Detected Execution Method: ${AUTO_EXEC}"
fi
## ****************************************************************************



###############################################################################
#### App Execution Preparation, Common to all Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to prepare environment for execution
MNT=""; ARG=""; CMD0="mkdir -p ${OUTPUT_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
MNT=""; ARG=""; CMD0="mkdir -p ${LOG_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
## ****************************************************************************



###############################################################################
#### App Execution, Specific to each Exec Method ####
###############################################################################

## ****************************************************************************
## Add logic to execute app
## There should be one case statement for each item in $exec_methods
case "${AUTO_EXEC}" in
    cdc-shared-singularity)
        MNT=""; ARG=""; ARG="${ARG} --variant"; MNT="${MNT} -B "; MNT="${MNT}\"${INPUT_DIR}:/data1\""; ARG="${ARG} \"/data1/${INPUT_BASE}\""; ARG="${ARG} --reference"; MNT="${MNT} -B "; MNT="${MNT}\"${REFERENCE_SEQUENCE_DIR}:/data2\""; ARG="${ARG} \"/data2/${REFERENCE_SEQUENCE_BASE}/${REFERENCE_SEQUENCE_BASE}.fasta\""; ARG="${ARG} --filter-expression"; ARG="${ARG} \"${FILTER_EXPRESSION}\""; ARG="${ARG} --filter-name"; ARG="${ARG} \"${FILTER_NAME}\""; ARG="${ARG} --output"; MNT="${MNT} -B "; MNT="${MNT}\"${OUTPUT_DIR}:/data5\""; ARG="${ARG} \"/data5/${OUTPUT_BASE}/${OUTPUT_BASE}.vcf\""; CMD0="singularity -s exec ${MNT} /apps/standalone/singularity/gatk/gatk-4.1.4.1-biocontainers.simg gatk VariantFiltration ${ARG}"; CMD0="${CMD0} >\"${LOG_FULL}/${OUTPUT_BASE}-gatk-variantfiltration.stdout\""; CMD0="${CMD0} 2>\"${LOG_FULL}/${OUTPUT_BASE}-gatk-variantfiltration.stderr\""; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
    singularity)
        MNT=""; ARG=""; ARG="${ARG} --variant"; MNT="${MNT} -B "; MNT="${MNT}\"${INPUT_DIR}:/data1\""; ARG="${ARG} \"/data1/${INPUT_BASE}\""; ARG="${ARG} --reference"; MNT="${MNT} -B "; MNT="${MNT}\"${REFERENCE_SEQUENCE_DIR}:/data2\""; ARG="${ARG} \"/data2/${REFERENCE_SEQUENCE_BASE}/${REFERENCE_SEQUENCE_BASE}.fasta\""; ARG="${ARG} --filter-expression"; ARG="${ARG} \"${FILTER_EXPRESSION}\""; ARG="${ARG} --filter-name"; ARG="${ARG} \"${FILTER_NAME}\""; ARG="${ARG} --output"; MNT="${MNT} -B "; MNT="${MNT}\"${OUTPUT_DIR}:/data5\""; ARG="${ARG} \"/data5/${OUTPUT_BASE}/${OUTPUT_BASE}.vcf\""; CMD0="singularity -s exec ${MNT} docker://quay.io/biocontainers/gatk4:4.1.4.1--1 gatk VariantFiltration ${ARG}"; CMD0="${CMD0} >\"${LOG_FULL}/${OUTPUT_BASE}-gatk-variantfiltration.stdout\""; CMD0="${CMD0} 2>\"${LOG_FULL}/${OUTPUT_BASE}-gatk-variantfiltration.stderr\""; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
esac
## ****************************************************************************



###############################################################################
#### Cleanup, Common to All Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to cleanup execution artifacts, if necessary
## ****************************************************************************

