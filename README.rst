GATK VariantFiltration GeneFlow App
===================================

Version: 4.1.4.0-01

This GeneFlow app wraps the GATK VariantFiltration tool.

Inputs
------

1. input: VCF file to be filtered.

2. reference_sequence: Directory that contains reference sequence fasta, index, and dict files.

Parameters
----------

1. filter_expression: Filter criteria. 

2. filter_name: Name of the filter. Default: filter.

3. output: Output directory - The name of the output directory to place VCF and VCF.idx files. Default: output.

